package com.inteligr8.alfresco.pdfmeta;

import java.io.File;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.alfresco.transformer.executors.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class PdfMetaTransformer implements Transformer {
	
	private final Logger logger = LoggerFactory.getLogger(PdfMetaTransformer.class);
	private final String id = "pdfmeta";
	
	private PdfTikaMetadataExtractor extractor;
	
	@PostConstruct
	public void init() throws Exception {
		if (this.logger.isDebugEnabled())
			this.logger.debug("init()");
		
		this.extractor = new PdfTikaMetadataExtractor();
	}
	
	@Override
	public String getTransformerId() {
		return this.id;
	}
	
	@Override
	public void extractMetadata(String transformName, String sourceMimetype, String targetMimetype, Map<String, String> transformOptions, File sourceFile, File targetFile) throws Exception {
		this.logger.trace("extractMetadata({}, {}, {}, {}, '{}', '{}')", transformName, sourceMimetype, targetMimetype, transformOptions, sourceFile, targetFile);

		if (!MediaType.APPLICATION_PDF_VALUE.equals(sourceMimetype))
			throw new IllegalArgumentException();
		if (transformOptions != null && !transformOptions.isEmpty())
			this.logger.debug("Transform options were specified, but they will be ignored: {}", transformOptions);
		
		this.extractor.extractMetadata(sourceMimetype, transformOptions, sourceFile, targetFile);
	}

}
