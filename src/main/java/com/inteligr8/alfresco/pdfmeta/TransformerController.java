/*
 * #%L
 * Alfresco Transform Core
 * %%
 * Copyright (C) 2005 - 2020 Alfresco Software Limited
 * %%
 * This file is part of the Alfresco software.
 * -
 * If the software was purchased under a paid Alfresco license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 * -
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * -
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * -
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 * 
 * Copyright (C) 2020 - 2021 Inteligr8
 */
package com.inteligr8.alfresco.pdfmeta;

import static org.alfresco.transformer.util.RequestParamMap.FILE;
import static org.alfresco.transformer.util.RequestParamMap.SOURCE_MIMETYPE;
import static org.alfresco.transformer.util.RequestParamMap.TARGET_EXTENSION;
import static org.alfresco.transformer.util.RequestParamMap.TARGET_MIMETYPE;
import static org.alfresco.transformer.util.RequestParamMap.TEST_DELAY;
import static org.alfresco.transformer.util.RequestParamMap.TRANSFORM_NAME_PROPERTY;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

import java.io.File;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.alfresco.transformer.AbstractTransformerController;
import org.alfresco.transformer.probes.ProbeTestTransform;
import org.alfresco.transformer.util.MimetypeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.multipart.MultipartFile;

/**
 * Controller for the Spring Boot transformer.
 *
 * Status Codes:
 *
 * 200 Success
 * 400 Bad Request: Request parameter <name> is missing (missing mandatory parameter)
 * 400 Bad Request: Request parameter <name> is of the wrong type
 * 400 Bad Request: Transformer exit code was not 0 (possible problem with the source file)
 * 400 Bad Request: The source filename was not supplied
 * 500 Internal Server Error: (no message with low level IO problems)
 * 500 Internal Server Error: The target filename was not supplied (should not happen as targetExtension is checked)
 * 500 Internal Server Error: Transformer version check exit code was not 0
 * 500 Internal Server Error: Transformer version check failed to create any output
 * 500 Internal Server Error: Could not read the target file
 * 500 Internal Server Error: The target filename was malformed (should not happen because of other checks)
 * 500 Internal Server Error: Transformer failed to create an output file (the exit code was 0, so there should be some content)
 * 500 Internal Server Error: Filename encoding error
 * 507 Insufficient Storage: Failed to store the source file
 */
@Controller
public class TransformerController extends AbstractTransformerController {
	
	private final Logger logger = LoggerFactory.getLogger(TransformerController.class);
	private final Pattern fileext = Pattern.compile("\\.([^\\.]+)$");

	@Autowired
	private PdfMetaTransformer transformer;
	
	@Value("${transform.pdfmeta.version}")
	private String version;
	
	private ProbeTestTransform probe;

	@Override
	public String getTransformerName() {
		return "pdfmeta";
	}

	@Override
	public String version() {
		return this.version;
	}
	
	@PostConstruct
	public void initProbe() {
		this.probe = new ProbeTestTransform(this, "quick.pdf", "quick", 
					7455L, 1024L, 150, 10240L, 60L * 20L + 1L, 60L * 15L - 15L) {
			@Override
			protected void executeTransformCommand(File sourceFile, File targetFile) {
				logger.trace("getProbeTestTransform().executeTransformCommand('{}', '{}')", sourceFile, targetFile);
				
				// FIXME
			}
		};
	}

	@Override
	public ProbeTestTransform getProbeTestTransform() {
		this.logger.trace("getProbeTestTransform()");
		return this.probe;
	}

	@Override
	protected String getTransformerName(final File sourceFile, final String sourceMimetype, final String targetMimetype, final Map<String, String> transformOptions) {
		this.logger.trace("getTransformerName('{}', {}, {}, {})", sourceFile, sourceMimetype, targetMimetype, transformOptions);
		// does not matter what value is returned, as it is not used because there is only one.
		return this.getTransformerName();
	}
	
	/**
	 * This override of simply makes targetExtension optional
	 */
	@Override
	@SuppressWarnings("deprecation")
    @PostMapping(value = "/transform", consumes = MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Resource> transform(HttpServletRequest request,
                                              @RequestParam(FILE) MultipartFile sourceMultipartFile,
                                              @RequestParam(value = TARGET_EXTENSION, required = false) String targetExtension,
                                              @RequestParam(value = SOURCE_MIMETYPE, required = false) String sourceMimetype,
                                              @RequestParam(value = TARGET_MIMETYPE, required = false) String targetMimetype,
                                              @RequestParam Map<String, String> requestParameters,
                                              @RequestParam (value = TEST_DELAY, required = false) Long testDelay,

                                              // The TRANSFORM_NAME_PROPERTY param allows ACS legacy transformers to specify which transform to use,
                                              // It can be removed once legacy transformers are removed from ACS.
                                              @RequestParam (value = TRANSFORM_NAME_PROPERTY, required = false) String requestTransformName) {
		if (targetExtension == null)
			targetExtension = "json";
		return super.transform(request, sourceMultipartFile, targetExtension, sourceMimetype, targetMimetype, requestParameters, testDelay, requestTransformName);
	}

	@Override
	public void transformImpl(String transformName, String sourceMimetype, String targetMimetype,  Map<String, String> transformOptions, File sourceFile, File targetFile) {
		this.logger.trace("transformImpl({}, {}, {}, {}, '{}', '{}')", transformName, sourceMimetype, targetMimetype, transformOptions, sourceFile, targetFile);
		
		if (sourceMimetype == null) {
			Matcher matcher = this.fileext.matcher(sourceFile.getAbsolutePath());
			sourceMimetype = matcher.find() ? this.ext2mime(matcher.group(1)) : null;
		}
		if (targetMimetype == null) {
			Matcher matcher = this.fileext.matcher(targetFile.getAbsolutePath());
			targetMimetype = matcher.find() ? this.ext2mime(matcher.group(1)) : MimetypeMap.MIMETYPE_METADATA_EXTRACT;
		}
		
		try {
			if (targetMimetype.equals(MimetypeMap.MIMETYPE_METADATA_EXTRACT)) {
				this.transformer.extractMetadata(transformName, sourceMimetype, targetMimetype, transformOptions, sourceFile, targetFile);
			} else {
				this.transformer.transform(transformName, sourceMimetype, targetMimetype, transformOptions, sourceFile, targetFile);
			}
		} catch (IllegalArgumentException iae) {
			throw new HttpClientErrorException(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private String ext2mime(String ext) {
		switch (ext.toLowerCase()) {
			// add applicable extensions here
			case "pdf":
			case "pdfa": return MediaType.APPLICATION_PDF_VALUE;
			default: return null;
		}
	}
}
