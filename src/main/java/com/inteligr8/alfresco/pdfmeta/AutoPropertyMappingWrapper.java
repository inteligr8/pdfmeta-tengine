package com.inteligr8.alfresco.pdfmeta;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inteligr8.alfresco.pdfmeta.util.DynamicDiscoveryMap;

public class AutoPropertyMappingWrapper extends DynamicDiscoveryMap<String, Set<String>> {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	public AutoPropertyMappingWrapper(Map<String, Set<String>> map) {
		super(map);
	}
	
	@Override
	protected void discoverExtraEntry(String key) {
		this.logger.trace("Looking up key as ACS property: {}", key);
		
		int colon = key != null ? key.indexOf(':') : -1;
		if (colon < 0) {
			// key is not formatted like an ACS property
			this.nullKeys.add(key);
		} else if (key.indexOf(':', colon+1) < 0) {
			logger.debug("Adding key/property to mapping: {}", key);
			this.extMap.put(key, Collections.singleton(key));
		} else {
			// 2 colons; not formatted like an ACS property
			this.nullKeys.add(key);
		}
	}

}
