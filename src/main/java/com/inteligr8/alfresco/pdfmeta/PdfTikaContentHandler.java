package com.inteligr8.alfresco.pdfmeta;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.dom4j.io.DOMSAXContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This is a custom implementation that extracts more meta-data from a PDF
 * than the built-in PDF extractor provided OOTB by Alfresco.  Namely, it
 * extracts the bookmarks, in addition to the property and text extraction
 * provided OOTB.
 * 
 * @author brian@inteligr8.com
 */
public class PdfTikaContentHandler extends DOMSAXContentHandler {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final XPathExpression xpathExprBodyPageText;
	private static final XPathExpression xpathExprBodyUl;
	private static final XPathExpression xpathExprLiText;
	private static final XPathExpression xpathExprText;
	
	private final Object parseBookmarkSync = new Object();
	private List<String> texts;
	private final Object parseTextSync = new Object();
	private Map<String, Serializable> bookmarks;
	
	static {
		XPath xpath = XPathFactory.newInstance().newXPath();
		xpath.setNamespaceContext(new TikaNamespaceContext());
		try {
			xpathExprBodyPageText = xpath.compile("/ns:html/ns:body/ns:div[@name=\"page\"]/text()");
			xpathExprBodyUl = xpath.compile("/ns:html/ns:body/ns:ul");
			xpathExprLiText = xpath.compile("ns:li/text()");
			xpathExprText = xpath.compile("text()");
		} catch (XPathExpressionException xpee) {
			throw new ExceptionInInitializerError(xpee);
		}
	}
	
	public String getTextByPage(int page) {
		this.parseText();
		return this.texts.get(page-1);
	}
	
	public List<String> getTextPerPages() {
		this.parseText();
		return this.texts;
	}
	
	private void parseText() {
		synchronized (this.parseTextSync) {
			if (this.texts == null) {
				try {
					this.texts = this.parseBodyForTexts(this.getDocument());
				} catch (XPathExpressionException xpee) {
					throw new IllegalStateException(xpee);
				}
			}
		}
	}
	
	public Map<String, Serializable> getBookmarks() {
		synchronized (this.parseBookmarkSync) {
			if (this.bookmarks == null) {
				try {
					this.bookmarks = this.parseBodyForBookmarks(this.getDocument());
				} catch (XPathExpressionException xpee) {
					throw new IllegalStateException(xpee);
				}
			}
		}
		
		return this.bookmarks;
	}
	
	private List<String> parseBodyForTexts(Document document) throws XPathExpressionException {
		NodeList pageTexts = (NodeList)xpathExprBodyPageText.evaluate(document, XPathConstants.NODESET);
		if (pageTexts == null || pageTexts.getLength() == 0)
			return null;
		return this.parseTexts(pageTexts);
	}
	
	private List<String> parseTexts(NodeList pageTexts) throws XPathExpressionException {
		List<String> texts = new LinkedList<>();
		
		for (int n = 0; n < pageTexts.getLength(); n++) {
			Node node = pageTexts.item(n);
			texts.add(node.getTextContent());
		}
		
		return texts;
	}
	
	private Map<String, Serializable> parseBodyForBookmarks(Document document) throws XPathExpressionException {
		this.logger.debug("Extracting bookmarks from the XML embedded in a PDF");
		Element element = (Element)xpathExprBodyUl.evaluate(document, XPathConstants.NODE);
		if (element == null)
			return null;
		return this.parseBookmarks(element);
	}
	
	private Map<String, Serializable> parseBookmarks(Element ulElement) throws XPathExpressionException {
		Map<String, Serializable> bookmarks = new LinkedHashMap<>();

		Element lastBookmarkKey = null;
		Element lastBookmarkValues = null;
		
		NodeList nodes = ulElement.getChildNodes();
		this.logger.debug("Found {} XML nodes; filtering down to just bookmarks ...", nodes.getLength());
		
		for (int n = 0; n < nodes.getLength(); n++) {
			Node node = nodes.item(n);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				if (node.getLocalName().equals("li")) {
					lastBookmarkKey = (Element)node;
				} else if (node.getLocalName().equals("ul")) {
					lastBookmarkValues = (Element)node;
				}
				
				if (lastBookmarkKey != null && lastBookmarkValues != null) {
					this.parseBookmark(lastBookmarkKey, lastBookmarkValues, bookmarks);
					lastBookmarkKey = null;
					lastBookmarkValues = null;
				}
			}
		}
		
		return bookmarks;
	}
	
	@SuppressWarnings("unchecked")
	private void parseBookmark(Element liElement, Element ulElement, Map<String, Serializable> bookmarks)
			throws XPathExpressionException {
		String bookmarkKey = (String)xpathExprText.evaluate(liElement, XPathConstants.STRING);
		
		Serializable bookmarkValue = bookmarks.get(bookmarkKey);
		
		NodeList nodes = (NodeList)xpathExprLiText.evaluate(ulElement, XPathConstants.NODESET);
		
		for (int n = 0; n < nodes.getLength(); n++) {
			Node node = nodes.item(n);
			this.logger.trace("Found bookmark value: {} => {}", bookmarkKey, node.getNodeValue());
			
			if (bookmarkValue == null) {
				bookmarks.put(bookmarkKey, node.getNodeValue());
			} else if (bookmarkValue instanceof List) {
				((List<Serializable>)bookmarkValue).add(node.getNodeValue());
			} else {
				LinkedList<Serializable> bookmarkValues = new LinkedList<>();
				bookmarkValues.add(bookmarkValue);
				bookmarkValues.add(node.getNodeValue());
				bookmarks.put(bookmarkKey, bookmarkValues);
			}
		}
	}
	
	
	
	private static class TikaNamespaceContext implements NamespaceContext {
		
		private Map<String, String> prefix2uri = new HashMap<>();
		private Map<String, String> uri2prefix = new HashMap<>();
		
		public TikaNamespaceContext() {
			this.prefix2uri.put("ns", "http://www.w3.org/1999/xhtml");
		}
		
		@Override
		public String getNamespaceURI(String prefix) {
			return this.prefix2uri.get(prefix);
		}
		
		@Override
		public String getPrefix(String namespaceURI) {
			return this.uri2prefix.get(namespaceURI);
		}
		
		@Override
		public Iterator<String> getPrefixes(String namespaceURI) {
			return Arrays.asList(this.uri2prefix.get(namespaceURI)).iterator();
		}
		
	}
	
}
