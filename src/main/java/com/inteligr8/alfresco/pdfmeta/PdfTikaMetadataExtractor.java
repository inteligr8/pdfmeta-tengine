package com.inteligr8.alfresco.pdfmeta;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.alfresco.transformer.metadataExtractors.PdfBoxMetadataExtractor;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.extractor.DocumentSelector;
import org.apache.tika.metadata.DublinCore;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.Office;
import org.apache.tika.metadata.PDF;
import org.apache.tika.metadata.Property;
import org.apache.tika.metadata.TikaCoreProperties;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PdfTikaMetadataExtractor extends PdfBoxMetadataExtractor {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected PdfTikaContentHandler newContentHandler() {
		return new PdfTikaContentHandler();
	}
	
	@Override
	public void mapMetadataAndWrite(File targetFile, Map<String, Serializable> metadata, Map<String, Set<String>> extractMapping) throws IOException {
		super.mapMetadataAndWrite(targetFile, metadata, new AutoPropertyMappingWrapper(extractMapping));
	}
	
	/**
	 * This is a copy of the AbstractTikaMetadataExtractor from ATS v2.3.6.
	 * {@link https://github.com/Alfresco/alfresco-transform-core/blob/2.3.6/alfresco-transform-tika/alfresco-transform-tika/src/main/java/org/alfresco/transformer/metadataExtractors/AbstractTikaMetadataExtractor.java}
	 */
    @Override
    public Map<String, Serializable> extractMetadata(String sourceMimetype, Map<String, String> transformOptions, File sourceFile) throws Exception {
		this.logger.trace("extractRaw({}, '{}')", sourceMimetype, sourceFile);
		
        Map<String, Serializable> rawProperties = new HashMap<>();

        InputStream istream = new FileInputStream(sourceFile);
        try {
            Parser parser = this.getParser();

            Metadata metadata = new Metadata();
            metadata.add(Metadata.CONTENT_TYPE, sourceMimetype);

            ParseContext context = this.buildParseContext(metadata, sourceMimetype);

            PdfTikaContentHandler handler = this.newContentHandler();

			this.logger.debug("Parsing {}b PDF using Apache Tika: {}", sourceFile.length(), sourceFile);
            parser.parse(istream, handler, metadata, context);
			this.logger.trace("Parsed {}b PDF using Apache Tika: {}", sourceFile.length(), sourceFile);
			this.logger.debug("Parsed PDF has meta-data: {}", Arrays.asList(metadata.names()));
			
			this.processMetadata(metadata, rawProperties);

			// this is the processing of that major difference
			this.processHandler(handler, rawProperties);
			if (this.logger.isTraceEnabled()) {
				this.logger.trace("Parsed PDF has properties: {}", rawProperties);
			} else if (this.logger.isTraceEnabled()) {
				this.logger.trace("Parsed PDF has properties: {}", rawProperties.keySet());
			}
        } finally {
        	istream.close();
        }

        return rawProperties;
    }

	/**
	 * This is a copy of the AbstractTikaMetadataExtractor from ATS v2.3.6.
	 * {@link https://github.com/Alfresco/alfresco-transform-core/blob/2.3.6/alfresco-transform-tika/alfresco-transform-tika/src/main/java/org/alfresco/transformer/metadataExtractors/AbstractTikaMetadataExtractor.java}
	 */
    private ParseContext buildParseContext(Metadata metadata, String sourceMimeType) {
        ParseContext context = new ParseContext();
        DocumentSelector selector = this.getDocumentSelector(metadata, sourceMimeType);
        if (selector != null)
            context.set(DocumentSelector.class, selector);
        return context;
    }

	/**
	 * This is a copy of the AbstractTikaMetadataExtractor from ATS v2.3.6.
	 * {@link https://github.com/Alfresco/alfresco-transform-core/blob/2.3.6/alfresco-transform-tika/alfresco-transform-tika/src/main/java/org/alfresco/transformer/metadataExtractors/AbstractTikaMetadataExtractor.java}
	 * 
	 * One slight difference is we are now using the Property object instead of
	 * the depreciated String object.
	 */
	protected Map<String, Serializable> processMetadata(Metadata metadata, Map<String, Serializable> rawProperties) {
		for (String tikaKey : metadata.names())
			this.putRawValue(tikaKey, this.getMetadataValue(metadata, tikaKey), rawProperties);
		
		this.putRawValue(KEY_TITLE, this.getMetadataValue(metadata, TikaCoreProperties.TITLE), rawProperties);
		this.putRawValue(KEY_COMMENTS, this.getMetadataValue(metadata, TikaCoreProperties.COMMENTS), rawProperties);
		this.putRawValue(KEY_TAGS, this.getMetadataValue(metadata, KEY_TAGS), rawProperties);
		this.putRawValue(KEY_SUBJECT, this.getMetadataValue(metadata, PDF.DOC_INFO_SUBJECT), rawProperties);
		this.putRawValue(KEY_DESCRIPTION, this.getMetadataValue(metadata, TikaCoreProperties.DESCRIPTION), rawProperties);
		this.putRawValue(KEY_CREATED, this.getMetadataValue(metadata, Property.composite(PDF.DOC_INFO_CREATED, new Property[] {DublinCore.CREATED, Office.CREATION_DATE, TikaCoreProperties.METADATA_DATE})), rawProperties);
		this.putRawValue(KEY_AUTHOR, this.getMetadataValue(metadata, Property.composite(PDF.DOC_INFO_CREATOR, new Property[] {DublinCore.CREATOR, Office.AUTHOR})), rawProperties);

		rawProperties = extractSpecific(metadata, rawProperties, null);
		return rawProperties;
	}
	
	protected void processHandler(PdfTikaContentHandler handler, Map<String, Serializable> rawProperties) {
		// TODO this should be more flexible than just a two-layer name/value pair
		
		Map<String, Serializable> bookmarks = handler.getBookmarks();
		if (bookmarks == null) {
			this.logger.debug("Found no bookmarks in PDF");
			return;
		}
		
		this.logger.debug("Found {} bookmarks in PDF", bookmarks.size());
		this.logger.trace("Found bookmarks in PDF: {}", bookmarks);
		
		rawProperties.putAll(bookmarks);
	}

	/**
	 * This method is all effectively copied/translated from Alfresco Community v6.2.
	 */
	private String getMetadataValue(Metadata metadata, String key) {
		if (metadata.isMultiValued(key)) {
			String[] parts = metadata.getValues(key);
			return StringUtils.trimToNull(this.getMetadataMultiValue(parts));
		} else {
			return StringUtils.trimToNull(metadata.get(key));
		}
    }

	/**
	 * This method is a mirror of the one above, but with a Property instead of
	 * String parameter.
	 */
	private String getMetadataValue(Metadata metadata, Property prop) {
		if (metadata.isMultiValued(prop)) {
			String[] parts = metadata.getValues(prop);
			return StringUtils.trimToNull(this.getMetadataMultiValue(parts));
		} else {
			return StringUtils.trimToNull(metadata.get(prop));
		}
    }

	/**
	 * This method is all effectively copied/translated from Alfresco Community v6.2.
	 */
	private String getMetadataMultiValue(String[] parts) {
		// use Set to prevent duplicates
		Set<String> value = new LinkedHashSet<>(parts.length);
		
		for (int i = 0; i < parts.length; i++)
		    value.add(parts[i]);
		
		String valueStr = value.toString();
		
		// remove leading/trailing braces []
		return valueStr.substring(1, valueStr.length() - 1);
	}

}
