package com.inteligr8.alfresco.pdfmeta.util;

import java.util.Set;

public class MultiSet<K> extends MultiCollection<K> implements Set<K> {
	
	public MultiSet(Set<K>[] sets) {
		super(sets);
	}

}
