package com.inteligr8.alfresco.pdfmeta.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class MultiCollection<E> implements Collection<E> {
	
	private final Collection<E>[] cs;
	
	public MultiCollection(Collection<E>[] cs) {
		this.cs = cs;
	}
	
	@Override
	public boolean contains(Object o) {
		for (Collection<E> list : this.cs)
			if (list.contains(o))
				return true;
		return false;
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		for (Object e : c)
			if (!this.contains(e))
				return false;
		return true;
	}
	
	@Override
	public boolean isEmpty() {
		for (Collection<E> list : this.cs)
			if (!list.isEmpty())
				return false;
		return true;
	}
	
	@Override
	public int size() {
		int count = 0;
		for (Collection<E> list : this.cs)
			count += list.size();
		return count;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {
			
			private Iterator<E> i = cs[0].iterator();
			private int arrayIndex = 0;
			
			@Override
			public boolean hasNext() {
				if (this.i == null)
					return false;
				if (this.i.hasNext())
					return true;
				while (this.arrayIndex < cs.length) {
					this.i = cs[this.arrayIndex++].iterator();
					if (this.i.hasNext())
						return true;
				}
				
				return false;
			}
			
			@Override
			public E next() {
				return this.i.next();
			}
			
			@Override
			public void remove() {
				this.i.remove();
			}
		};
	}
	
	@Override
	public Object[] toArray() {
		Object[] array = new Object[this.size()];
		int i = 0;
		for (E o : this)
			array[i++] = o;
		return array;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a) {
		int size = this.size();
		if (a == null || a.length < size)
			a = Arrays.copyOf(a, size);

		int i = 0;
		for (E o : this)
			a[i++] = (T)o;
		return a;
	}
	
	@Override
	public boolean add(E e) {
		return this.cs[0].add(e);
	}
	
	@Override
	public boolean addAll(Collection<? extends E> c) {
		return this.cs[0].addAll(c);
	}
	
	@Override
	public boolean remove(Object o) {
		for (Collection<E> set : this.cs)
			if (set.remove(o))
				return true;
		return false;
	}
	
	@Override
	public boolean removeAll(Collection<?> c) {
		boolean changed = false;
		for (Object e : c)
			changed = this.remove(e) || changed;
		return changed;
	}
	
	@Override
	public boolean retainAll(Collection<?> c) {
		boolean changed = false;
		for (Collection<E> set : this.cs)
			changed = set.retainAll(c) || changed;
		return changed;
	}
	
	@Override
	public void clear() {
		for (Collection<E> set : this.cs)
			set.clear();
	}

}
