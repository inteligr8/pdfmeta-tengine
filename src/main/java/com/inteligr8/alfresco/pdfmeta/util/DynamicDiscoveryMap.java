package com.inteligr8.alfresco.pdfmeta.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class DynamicDiscoveryMap<K, V> implements Map<K, V> {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private final Map<K, V> underlyingMap;
	protected final Map<K, V> extMap = new HashMap<>();
	protected final Set<K> nullKeys = new HashSet<>();
	
	public DynamicDiscoveryMap(Map<K, V> map) {
		this.underlyingMap = map;
	}
	
	@SuppressWarnings("unchecked")
	private K castKey(Object key) {
		return (K)key;
	}
	
	@Override
	public boolean containsKey(Object key) {
		if (this.underlyingMap.containsKey(key) ||
				this.extMap.containsKey(key))
			return true;
		
		if (this.nullKeys.contains(key))
			return false;
		
		this.discoverExtraEntry(this.castKey(key));
		return this.extMap.containsKey(key);
	}
	
	@Override
	public boolean containsValue(Object value) {
		return this.underlyingMap.containsValue(value) ||
				this.extMap.containsValue(value);
	}
	
	@Override
	public boolean isEmpty() {
		return this.underlyingMap.isEmpty() && this.extMap.isEmpty();
	}
	
	@Override
	public int size() {
		return this.underlyingMap.size() + this.extMap.size();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Set<K> keySet() {
		Set<K>[] sets = (Set<K>[])Array.newInstance(this.underlyingMap.keySet().getClass(), 2);
		sets[0] = this.underlyingMap.keySet();
		sets[1] = this.extMap.keySet();
		return new MultiSet<>(sets);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Entry<K, V>> entrySet() {
		Set<Entry<K, V>>[] sets = (Set<Entry<K, V>>[])Array.newInstance(this.underlyingMap.entrySet().getClass(), 2);
		sets[0] = this.underlyingMap.entrySet();
		sets[1] = this.extMap.entrySet();
		return new MultiSet<>(sets);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<V> values() {
		Collection<V> values = this.underlyingMap.values();
		Collection<V>[] cs = (Collection<V>[])Array.newInstance(values.getClass(), 2);
		cs[0] = values;
		cs[1] = this.extMap.values();
		return new MultiCollection<>(cs);
	}
	
	@Override
	public V get(Object key) {
		if (this.underlyingMap.containsKey(key))
			return this.underlyingMap.get(key);
		if (!this.extMap.containsKey(key)) {
			this.logger.debug("Discover possible extra key: {}", key);
			this.discoverExtraEntry(this.castKey(key));
		}
		return this.extMap.get(key);
	}
	
	@Override
	public V put(K key, V value) {
		return this.underlyingMap.put(key, value);
	}
	
	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		this.underlyingMap.putAll(m);
	}
	
	@Override
	public V remove(Object key) {
		this.nullKeys.remove(key);
		V values1 = this.extMap.remove(key);
		V values2 = this.underlyingMap.remove(key);
		return values2 != null ? values2 : values1;
	}
	
	@Override
	public boolean remove(Object key, Object value) {
		boolean b1 = this.underlyingMap.remove(key, value);
		boolean b2 = this.extMap.remove(key, value);
		return b1 || b2;
	}
	
	@Override
	public void clear() {
		this.underlyingMap.clear();
		this.extMap.clear();
		this.nullKeys.clear();
	}
	
	protected abstract void discoverExtraEntry(K key);
	
}
